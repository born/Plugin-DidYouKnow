#ifndef DIDYOUKNOWPLUGIN_HH
#define DIDYOUKNOWPLUGIN_HH

#include <OpenFlipper/BasePlugin/BaseInterface.hh>

#include "DidYouKnowWidget.hh"

class DidYouKnowPlugin : public QObject, BaseInterface
{
    Q_OBJECT
    Q_INTERFACES(BaseInterface)

#if QT_VERSION >= 0x050000
    Q_PLUGIN_METADATA(IID "org.OpenFlipper.Plugins.Plugin-DidYouKnow")
#endif

signals:

public:
    DidYouKnowPlugin();
    ~DidYouKnowPlugin();

    // BaseInterface
    QString name() { return QString("Did You Know?"); }
    QString description() { return QString("Displays a Welcome screen with helpful tips and hints about OpenFlipper and geometry processing in general."); }

private slots:
    // BaseInterface
    void pluginsInitialized();

public slots:
    QString version() { return QString("1.0"); }

private:
    DidYouKnowWidget* mDidYouKnowWidget;
};

#endif //DIDYOUKNOWPLUGIN_HH
