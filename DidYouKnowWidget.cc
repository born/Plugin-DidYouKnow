#include "DidYouKnowWidget.hh"

#include <OpenFlipper/common/GlobalOptions.hh>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QStandardPaths>
#include <QDateTime>

#include <QNetworkAccessManager>
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>

#include <iostream>

DidYouKnowWidget::DidYouKnowWidget(QWidget* _parent) :
    QDialog(_parent)
{
    setWindowTitle("Tip of the Day");
    setWindowFlags(Qt::Dialog | Qt::WindowTitleHint | Qt::WindowCloseButtonHint);

    mVBoxLayoutGlobal = new QVBoxLayout();
    mHBoxLayoutBottomRow = new QHBoxLayout();

    QSize labelSize(420, 200);
    mLabelTip = new QLabel();
    mLabelTip->setText("Did you know...");
    mLabelTip->setMinimumSize(labelSize);
    mLabelTip->setMaximumSize(labelSize);
    mLabelTip->setAlignment(Qt::AlignTop);
    mLabelTip->setWordWrap(true);

    mCheckBoxShowTipsOnStartup = new QCheckBox();
    mCheckBoxShowTipsOnStartup->setText("&Show tips at startup");
    mCheckBoxShowTipsOnStartup->setChecked(true);

    mPushButtonNextTip = new QPushButton();
    mPushButtonNextTip->setText("&Tell me more!");
    connect(mPushButtonNextTip, SIGNAL(clicked(bool)), this, SLOT(displayNextTip()));

    mPushButtonClose = new QPushButton();
    mPushButtonClose->setText("Thank y&ou!");
    connect(mPushButtonClose, SIGNAL(clicked(bool)), this, SLOT(close()));

    mHBoxLayoutBottomRow->addWidget(mCheckBoxShowTipsOnStartup);
    mHBoxLayoutBottomRow->addWidget(mPushButtonNextTip);
    mHBoxLayoutBottomRow->addWidget(mPushButtonClose);

    mVBoxLayoutGlobal->addWidget(mLabelTip);
    mVBoxLayoutGlobal->addLayout(mHBoxLayoutBottomRow);

    setLayout(mVBoxLayoutGlobal);
    setFixedSize(sizeHint());

    displayEmptyTip();
    try {
        updateTips();
        displayRandomTip();
    } catch (ConfigError& e) {
        std::cout << "Could not display tip of the day: " << e.what() << std::endl;
    }
}

QString DidYouKnowWidget::libraryFileName() const {
    QString configPath = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);
    QString path = QDir(configPath).filePath("didYouKnowLibrary.json");
    return path;
}

void DidYouKnowWidget::updateTips()
{
    // Number of seconds after which the local tips library is considered outdated
    const int outdatedSeconds = 1 * 24 * 60 * 60;
    const QUrl libraryURL("https://graphics.rwth-aachen.de:9000/born/Plugin-DidYouKnow/raw/master/Library/tips.json");

    // Check for existing local library file
    QFileInfo libraryFileInfo(libraryFileName());
    bool downloadNewLibrary = false;
    if (!libraryFileInfo.exists()) {
        downloadNewLibrary = true;
    }
    else if (libraryFileInfo.lastModified().secsTo(QDateTime::currentDateTime()) > outdatedSeconds) {
        downloadNewLibrary = true;
    }
    if (downloadNewLibrary) {
        QNetworkAccessManager manager;
        QNetworkRequest request(libraryURL);
        QNetworkReply* reply = manager.get(request);

        QEventLoop loop;
        connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
        loop.exec();

        if (reply->error() == QNetworkReply::NoError) {
            QByteArray body = reply->readAll();
            QFile libraryFile(libraryFileName());
            if (!libraryFile.open(QIODevice::WriteOnly)) {
                throw ConfigError("Could not open " + libraryFile.fileName().toStdString());
            }
            libraryFile.write(body);
        }
    }

    mTips.clear();
    QFile libraryFile(libraryFileName());
    if (!libraryFile.open(QIODevice::ReadOnly)) {
        // Try library file from local repository as fallback
        libraryFile.setFileName(":Plugin-DidYouKnow/Library/tips.json");
        if (!libraryFile.open(QIODevice::ReadOnly)) {
            throw ConfigError("Could not open " + libraryFile.fileName().toStdString());
        }
    }
    QByteArray libraryData = libraryFile.readAll();
    QJsonDocument libraryDoc = QJsonDocument::fromJson(libraryData);
    if (!libraryDoc.isObject()) {
        throw ConfigError("Could not load JSON data");
    }
    QJsonObject libraryObj = libraryDoc.object();
    if (!libraryObj["tips"].isArray()) {
        throw ConfigError("Could not find JSON array named 'tips' on top level");
    }
    QJsonArray tips = libraryObj["tips"].toArray();
    for (QJsonArray::Iterator it = tips.begin(); it != tips.end(); ++it) {
        if (!(*it).isString()) {
            throw ConfigError("Element of 'tips' array is not a string");
        }
        mTips.push_back((*it).toString());
    }
    std::random_shuffle(mTips.begin(), mTips.end());
}

void DidYouKnowWidget::displayTip(std::size_t _tipIndex)
{
    displayEmptyTip();
    if (_tipIndex < mTips.size()) {
        QString text = mLabelTip->text();
        text += mTips[_tipIndex];
        mLabelTip->setText(text);
    }
}

void DidYouKnowWidget::displayEmptyTip()
{
    QString text;
    text += "<h1><img src=\"" + OpenFlipper::Options::iconDirStr() + OpenFlipper::Options::dirSeparator() + "lightbulb.png\"> Did you know...</h1>";
    text += "<hr>";
    mLabelTip->setText(text);
}

void DidYouKnowWidget::displayRandomTip()
{
    mCurrentTipIndex = rand() % mTips.size();
    displayTip(mCurrentTipIndex);
}

void DidYouKnowWidget::displayNextTip()
{
    mCurrentTipIndex = (mCurrentTipIndex + 1) % mTips.size();
    displayTip(mCurrentTipIndex);
}

