#include "DidYouKnowPlugin.hh"

DidYouKnowPlugin::DidYouKnowPlugin() :
    mDidYouKnowWidget(new DidYouKnowWidget())
{
}

DidYouKnowPlugin::~DidYouKnowPlugin()
{
}

void DidYouKnowPlugin::pluginsInitialized() {
    mDidYouKnowWidget->show();
    mDidYouKnowWidget->raise();
    mDidYouKnowWidget->activateWindow();
}

#if QT_VERSION < 0x050000
  Q_EXPORT_PLUGIN2(didyouknowplugin, DidYouKnowPlugin);
#endif
