# Plugin-DidYouKnow

This plugin displays a popup window with helpful tips and fun facts to simplify and enrich a geometer's everyday work with OpenFlipper.

![Screenshot](screenshot.png "Screenshot")

## Adding your own tips

Feel free to extend the tip of the day library by sharing your wisdom!
In order to add a new tip, just edit [the library file](https://graphics.rwth-aachen.de:9000/born/Plugin-DidYouKnow/blob/master/Library/tips.json).
(If you have master access, you can edit the file directly through GitLab without even cloning the repository!)
Make sure to use valid JSON format.
