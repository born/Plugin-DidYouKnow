#ifndef DIDYOUKNOWWIDGET_HH
#define DIDYOUKNOWWIDGET_HH

#include <QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QCheckBox>
#include <QLabel>

#include <stdexcept>

class DidYouKnowWidget : public QDialog
{
    Q_OBJECT

public:
    explicit DidYouKnowWidget(QWidget* _parent = 0);

private:
    struct ConfigError : public std::runtime_error
    {
        ConfigError(const std::string& _what) :
            std::runtime_error(_what)
        {
        }
    };

    QString libraryFileName() const;

private slots:
    void updateTips();
    void displayTip(std::size_t _tipIndex);
    void displayEmptyTip();
    void displayRandomTip();
    void displayNextTip();

private:
    std::vector<QString> mTips;
    std::size_t mCurrentTipIndex;

    QVBoxLayout* mVBoxLayoutGlobal;
    QHBoxLayout* mHBoxLayoutBottomRow;
    QLabel*      mLabelTip;
    QPushButton* mPushButtonNextTip;
    QPushButton* mPushButtonClose;
    QCheckBox*   mCheckBoxShowTipsOnStartup;
};

#endif //DIDYOUKNOWWIDGET_HH
